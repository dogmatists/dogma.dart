/* This is free and unencumbered software released into the public domain. */

/// An angle.
class Angle {
  final double radians;

  const Angle(this.radians);
}
