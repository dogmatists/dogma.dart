# Dogma for Dart

[![Project license](https://img.shields.io/badge/license-Public%20Domain-blue.svg)](https://unlicense.org)
[![Pub package](https://img.shields.io/pub/v/dogma.svg)](https://pub.dev/packages/dogma)
